#Project 2 Main File
#Team 344
import matplotlib.pyplot as plt
import numpy as np
import movefile
import sys
#overwrite issue with car class
#from movefile import move

class Car(object):
    def __init__(self, position = 0, velocity = 1.0):
        self.position = position
        self.velocity = velocity
    #def place(self):  
    def move(self, car2position, override=False):
	results = movefile.move(self.position, car2position, self.velocity, override)
	self.velocity = results[0]
	self.position = results[1]

#############################################################################################################

#initialize car list
car_list = [Car()]
#set timer
t = 0
n_cars = 1
while len(car_list) > 0:
	t = t+1
	#use simple highway list with true for deer, false for no deer
	highway_list = []
	for i in range(100):
		randomnum = np.random.random()
		if randomnum < 0.02:
			highway_list.append(True)
		else:
			highway_list.append(False)

	#remove any car over 100 position
	new_list = []
        for i in range(len(car_list)):
	        if car_list[i].position < 100.0: new_list.append(car_list[i])
	car_list = new_list
        
        if 200 < t < 220:
            deer_list = []
            matrix = np.zeros((1,10))
            for i in range(len(car_list)):
                if highway_list[int(car_list[i].position)] == True:
                    deer_list.append(car_list[i].position)
            for k in range(len(deer_list)):
                matrix[0,k] = deer_list[k]


        velocity_list = []
        position_list = []
	for i in range(len(car_list)):
		#slow all cars by half behind a mile marker with a deer
        	if highway_list[int(car_list[i].position)] == True:
                    for j in car_list[i:]:
                        j.velocity = j.velocity*0.25
                    car_list[i].move(car_list[i-1].position, override=True)
		#if the car is the first one on the road, set no car in front
		elif i == 0:
			car_list[i].move(200)
		#otherwise, move car according to position of car in front
		else:
			car_list[i].move(car_list[i-1].position)
                velocity_list.append(car_list[i].velocity)
                position_list.append(car_list[i].position)

        if 200 < t < 220:
            #print 'NEW RUN'
            #ax = plt.axes()
            fig, ax1 = plt.subplots()
            ax2 = ax1.twinx()
            ax3 = ax1.twinx()
            ax4 = ax1.twinx()
            ax5 = ax1.twinx()
            ax6 = ax1.twinx()
            ax7 = ax1.twinx()
            ax8 = ax1.twinx()
            ax9 = ax1.twinx()
            ax10 = ax1.twinx()
            ax11 = ax1.twinx()
            for i in range(len(car_list)):
                ax1.arrow(position_list[i],0,velocity_list[i],0, head_width = 0.05)
                ax2.plot([matrix[0,0], matrix[0,0]], [-1, 1],'r')
                ax3.plot([matrix[0,1], matrix[0,1]], [-1, 1],'r')
                ax4.plot([matrix[0,2], matrix[0,2]], [-1, 1],'r')
                ax5.plot([matrix[0,3], matrix[0,3]], [-1, 1],'r')
                ax6.plot([matrix[0,4], matrix[0,4]], [-1, 1],'r')
                ax7.plot([matrix[0,5], matrix[0,5]], [-1, 1],'r')
                ax8.plot([matrix[0,6], matrix[0,6]], [-1, 1],'r')
                ax9.plot([matrix[0,7], matrix[0,7]], [-1, 1],'r')
                ax10.plot([matrix[0,8], matrix[0,8]], [-1, 1],'r')
                ax11.plot([matrix[0,9], matrix[0,9]], [-1, 1],'r')
            ax1.set_ylim(-1,1)
            ax1.set_xlim(1,100)
            ax1.set_title('Car Velocities and Deer Positions at t = %s' %t)
            plt.savefig('velocity_%s.png' %t)
            plt.close('all')

	if n_cars < 1000 and car_list[-1].position > 1.0:
		car_list.append(Car(position=car_list[-1].position-1.0))
                n_cars += 1
        #print n_cars
#print t
#plt.savefig('plot.png')

#total time vs deer probability -Tristan
#average velocity vs time -Tristan
#velocity vs position (stepped) with deer line -Larkin
#velocity vectors for first ten cars -Emery
