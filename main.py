#Project 2 Main File
#Team 344

import numpy as np
import movefile
import sys
#overwrite issue with car class
#from movefile import move

class Car(object):
    def __init__(self, position = 0, velocity = 1.0):
        self.position = position
        self.velocity = velocity
    #def place(self):  
    def move(self, car2position, override=False):
	results = movefile.move(self.position, car2position, self.velocity, override)
	self.velocity = results[0]
	self.position = results[1]

#############################################################################################################

#initialize car list
car_list = [Car()]
#set timer
t = 0
n_cars = 1
while len(car_list) > 0:
	t = t+1
	#use simple highway list with true for deer, false for no deer
	highway_list = []
	for i in range(100):
		randomnum = np.random.random()
		if randomnum < 0.02:
			highway_list.append(True)
		else:
			highway_list.append(False)

	#remove any car over 100 position
	new_list = []
        for i in range(len(car_list)):
	        if car_list[i].position < 100.0: new_list.append(car_list[i])
	car_list = new_list


	for i in range(len(car_list)):
		#slow all cars by half behind a mile marker with a deer
        	if highway_list[int(car_list[i].position)] == True:
	        	for j in car_list[i:]:
				j.velocity = j.velocity*0.5
			car_list[i].move(car_list[i-1].position, override=True)
		#if the car is the first one on the road, set no car in front
		elif i == 0:
			car_list[i].move(200)
		#otherwise, move car according to position of car in front
		else:
			car_list[i].move(car_list[i-1].position)

	if n_cars < 1000 and car_list[-1].position > 1.0:
		car_list.append(Car(position=car_list[-1].position-1.0))
                n_cars += 1
        print n_cars
print t

#The function that repeats the above while loop for "trials" times.#
def func(trials):
	r = 0
	time = []
	deer = []
	average = []

	while r < trials:
		deers = 0
		speed = [0]
		
		#initialize car list
		car_list = [Car()]
		#set timer
		t = 0
		n_cars = 1
		while len(car_list) > 0:
			t = t+1
			#use simple highway list with true for deer, false for no deer
			highway_list = []
			for i in range(100):
				randomnum = np.random.random()
				if randomnum < 0.01*r:
					highway_list.append(True)
				else:
					highway_list.append(False)

			#remove any car over 100 position
			new_list = []
		        for i in range(len(car_list)):
			        if car_list[i].position < 100.0: new_list.append(car_list[i])
			car_list = new_list


			for i in range(len(car_list)):
				#slow all cars by half behind a mile marker with a deer
		        	if highway_list[int(car_list[i].position)] == True:
			        	for j in car_list[i:]:
						j.velocity = j.velocity*0.5
					car_list[i].move(car_list[i-1].position, override=True)
				#if the car is the first one on the road, set no car in front
				elif i == 0:
					car_list[i].move(200)
				#otherwise, move car according to position of car in front
				else:
					car_list[i].move(car_list[i-1].position)

			if n_cars < 1000 and car_list[-1].position > 1.0:
				car_list.append(Car(position=car_list[-1].position-1.0))
		                n_cars += 1
		    
			deers += highway_list.count(True)
			if len(car_list) > 0:
				speed.append(sum(i.velocity for i in car_list[0:])/len(car_list))

		deer.append(deers)
		average.append(sum(i for i in speed)/len(speed))
		time.append(t)
		r += 1
	
	return time, deer, average

#total time vs deer probability -Tristan
import matplotlib.pyplot as plt
trials = 23
plt.figure()
t = func(trials)[0]
d = func(trials)[1]
plt.plot(d, t, 'bo')
plt.ylabel('Total Time')
plt.xlabel('Total Occurrences of Deer')
plt.show()
#average velocity vs time -Tristan
plt.figure()
a = func(trials)[2]
plt.plot(a, t, 'ro')
plt.ylabel('Time for All Cars to Pass')
plt.xlabel('Average Velocity of All Cars')
plt.show()
#velocity vs position (stepped) with deer line -Larkin
#velocity vectors for first ten cars -Emery
